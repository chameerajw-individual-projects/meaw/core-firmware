#define SIM800_RX           2 //sim800 TX pin to pin 2. Arduino RX
#define SIM800_TX           3 //sim800 RX pin to pin 3. Arduino TX
#define SIM800_RST          4

// HX711 circuit wiring
#define LOADCELL_DOUT_PIN   7
#define LOADCELL_SCK_PIN    6

#define SERIAL_RX_HOST      9 //Receiver pin of this IC
#define SERIAL_TX_HOST      10 //Transmitter pin of this IC

#define SELECT_PIN          11
#define UP_PIN              12
#define DOWN_PIN            13
#define CANCEL_PIN          14 //A0

typedef enum {S_INITIAL, S_CHECKEEPROM, S_CHECKINTERNET, S_SENDEEPROM, S_SENDORDELETE, S_CONFIRMDELETE, S_DELETESTATE, S_SELECTESTATE, S_UPDATEWEIGHT, S_CONFIRMSEND, S_SENDSTATE} stateMachineState_t;
typedef enum {INITIAL, INTERNET, SENDORDELETE, CONFIRMDELETE, DELETESTATE, SELECTESTATE, UPDATEWEIGHT, CONFIRMSEND, SENDSTATE, BATTERY, SIGNAL, TIME, DEVICEID, PACKETCOUNT} displayState_t;
typedef enum {IDLING, SELECT, UP, DOWN, CANCEL} buttonCmd_t;

typedef enum {FAILCONNECT, CONNECTED, CONNECT} internet_t;
typedef enum {SEND, DELETE} sendOrDelete_t;
typedef enum {DELETEYES, DELETENO} confirmDelete_t;
typedef enum {DELETING, DELETED} deleteState_t;
typedef enum {LOCK, TARE} updateWeight_t;
typedef enum {SENDYES, SENDNO} confirmSend_t;
typedef enum {SENDING, SENT, SAVED} sendState_t;

String estate_name_array[5] = {"Temp Estate 01", "Temp estate 02", "Temp estate 03", "Temp estate 04", "Temp estate 05"};

void setup() {
  Serial.begin(115200);

  initWeight();
  
  initSwitches();
  
  updateEemVars();

  initDisplay();

  rtcInit();
  
  //initCom();
  //establishCom();
  //sendGsm();
  
  
}

void loop() {
  //printWeight();
  //sendDisplay();
  //getNowTime();
  //delay(1000);
    
  stateMachine();
}
