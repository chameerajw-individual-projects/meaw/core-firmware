#include <EEPROM.h>

#define EEM_TOTAL_SIZE            1024
#define EEM_DEVICE_ID             0
#define EEM_TOTAL_PACKET_COUNT    400
#define EEM_TOTAL_PACKET_FOR_DAY  404
#define EEM_SAVED_PACKET_FOR_DAY  406

#define EEM_PACKET_HH             500
#define EEM_PACKET_MM             550
#define EEM_PACKET_ESTATE         600
#define EEM_PACKET_WEIGHT         650

uint16_t device_ID = 0;
uint16_t total_packet_count = 0;
uint8_t total_packet_for_day = 0;
uint8_t saved_packet_for_day = 0;
uint8_t sent_packet_for_day = 0;

void updateEemVars(void)
{
  EEPROM.get(EEM_DEVICE_ID, device_ID);
  EEPROM.get(EEM_TOTAL_PACKET_COUNT, total_packet_count);
  EEPROM.get(EEM_TOTAL_PACKET_FOR_DAY, total_packet_for_day);
  EEPROM.get(EEM_SAVED_PACKET_FOR_DAY, saved_packet_for_day);
  sent_packet_for_day = total_packet_for_day - saved_packet_for_day;
}
