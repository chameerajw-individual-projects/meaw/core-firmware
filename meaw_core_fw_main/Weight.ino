#include "HX711.h"

HX711 scale;

void initWeight(void)
{  
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
}

void printWeight(void)
{
  if (scale.is_ready()) {
    long reading = scale.read();
    Serial.print("HX711 reading: ");
    Serial.println(reading);
  } else {
    Serial.println("HX711 not found.");
  }
}
