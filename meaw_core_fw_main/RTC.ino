#include <Wire.h>
#include <RtcDS3231.h> //refer rtc_minute_timer on component test directory

RtcDS3231<TwoWire> rtcObject(Wire); //RTC object

void rtcInit(void)
{
  rtcObject.Begin(); //start RTC

  //RtcDateTime timestamp = RtcDateTime(__DATE__, __TIME__);
  //rtcObject.SetDateTime(timestamp);

  //rtcObject.SetDateTime(RtcDateTime(2020, 9, 28, 0, 0, 0)); //set time of RTC

  //delay(1000);

  RtcDateTime timestamp = rtcObject.GetDateTime();
  char time[10];

  sprintf(time, "%d:%d:%d",
          timestamp.Hour(),
          timestamp.Minute(),
          timestamp.Second()
         );
  Serial.println(time);
}

/*char getNowTime(void)
{
  char timeArray[10];
  RtcDateTime nowTimestamp = rtcObject.GetDateTime();
  //char nowTime[10];
  sprintf(timeArray, "%d:%d:%d",
          nowTimestamp.Hour(),
          nowTimestamp.Minute(),
          nowTimestamp.Second()
         );

  return timeArray;
}*/

void getNowTime(void)
{
  RtcDateTime timestamp = rtcObject.GetDateTime();
  char time[10];

  sprintf(time, "%d:%d:%d",
          timestamp.Hour(),
          timestamp.Minute(),
          timestamp.Second()
         );
  Serial.println(time);
}
