stateMachineState_t machine_state = S_INITIAL;

void stateMachine(void)
{
  switch (machine_state)
  {
    case (S_INITIAL):
      disp_packetCount(total_packet_for_day, sent_packet_for_day, saved_packet_for_day);
      machine_state = S_CHECKINTERNET;
      break;

    case (S_CHECKINTERNET):
      disp_connectingToInternet(CONNECT);
      is_internet_connected = com_getSignalState();
      (is_internet_connected == true) ? disp_connectingToInternet(CONNECTED) : disp_connectingToInternet(FAILCONNECT);
      Serial.println("Here");
      disp_updateNetwork(is_internet_connected);
      Serial.println("Here 1");
      delay(2000);
      //machine_state = S_CHECKEEPROM;
      machine_state = S_SENDORDELETE;
      break;

    case (S_CHECKEEPROM):
      Serial.println("Here 2");
      ((is_internet_connected == true) && (saved_packet_for_day > 0)) ? machine_state = S_SENDEEPROM : machine_state = S_SENDORDELETE;
      Serial.println("Here 3");
      break;

    case (S_SENDEEPROM):
      sendEemPackets();
      machine_state = S_SENDORDELETE;
      break;

    case (S_SENDORDELETE):
      Serial.println("Here 4");
      disp_sendOrDelete(SEND);
      int current_selection = 0;
      int old_selection = 0;
      buttonCmd_t btnCmd = IDLING;
      Serial.println("Here 5");
      while (true)
      {
        btnCmd = getButtonClick(2, &current_selection);
        if(old_selection != current_selection)
        {
          disp_sendOrDelete(current_selection);
        }   
        if(btnCmd == SELECT)
        {
          (current_selection == SEND)? machine_state = S_SELECTESTATE: S_CONFIRMDELETE;
          break;
        }
      }
      break;

    case (S_CONFIRMDELETE):
      disp_deleteConfirmation(DELETEYES, "TEMP", "12.344", "23:23");
      break;

    case (S_DELETESTATE):

      break;

    case (S_SELECTESTATE):
      disp_selectEstate("TEMP");
      break;

    case (S_UPDATEWEIGHT):

      break;

    case (S_CONFIRMSEND):

      break;

    case (S_SENDSTATE):

      break;
  }
}

void sendEemPackets(void)
{
  uint8_t initial_saved_packet_for_day = saved_packet_for_day;
  for (uint8_t i = 0 ; i < initial_saved_packet_for_day; i++)
  {
    uint8_t packet_hh, packet_mm, packet_estate = 0;
    float packet_weight = 0.0;

    EEPROM.get(EEM_PACKET_HH + (i * sizeof(uint8_t)), packet_hh);
    EEPROM.get(EEM_PACKET_MM + (i * sizeof(uint8_t)), packet_mm);
    EEPROM.get(EEM_PACKET_ESTATE + (i * sizeof(uint8_t)), packet_estate);
    EEPROM.get(EEM_PACKET_WEIGHT + (i * sizeof(float)), packet_weight);

    bool ret_state = false;
    String timestampStr = String(String(packet_hh) + ':' + String(packet_mm));
    disp_sendState(SENDING, estate_name_array[packet_estate], String(packet_weight, 3), timestampStr);
    ret_state = com_sendPacket(estate_name_array[packet_estate], String(packet_weight, 3), timestampStr);
    if (ret_state == true)
    {
      disp_sendState(SENT, estate_name_array[packet_estate], String(packet_weight, 3), timestampStr);
      sent_packet_for_day ++;
      saved_packet_for_day --;
      disp_packetCount(total_packet_for_day, sent_packet_for_day, saved_packet_for_day);
      delay(1000);
    }
    else
    {
      disp_sendState(SAVED, estate_name_array[packet_estate], String(packet_weight, 3), timestampStr);
      delay(1000);
      break;
    }
  }
}
