void initSwitches(void)
{
  pinMode(SELECT_PIN, INPUT);
  pinMode(UP_PIN, INPUT);
  pinMode(DOWN_PIN, INPUT);
  pinMode(CANCEL_PIN, INPUT);
}

buttonCmd_t getButtonClick(uint8_t max_sel, int *current_sel)
{
  if(digitalRead(SELECT_PIN) == 0)
  {
    return SELECT;
  }
  else if(digitalRead(UP_PIN) == 0)
  {
    *current_sel = min(*current_sel + 1, max_sel - 1);
    return UP;
  }
  else if(digitalRead(DOWN_PIN) == 0)
  {
    *current_sel = max(0, *current_sel - 1);
    return DOWN;
  }
  else if(digitalRead(CANCEL_PIN) == 0)
  {
    return CANCEL;
  }  
  return IDLING;
}
