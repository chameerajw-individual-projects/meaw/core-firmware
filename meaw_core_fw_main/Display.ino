#include <SoftwareSerial.h>
#include <ArduinoJson.h>
//https://arduinojson.org/v6/how-to/do-serial-communication-between-two-boards/

SoftwareSerial serialDisplay(SERIAL_RX_HOST, SERIAL_TX_HOST); // RX, TX

displayState_t common_display_state = INITIAL;

void initDisplay()
{  
  serialDisplay.begin(4800);
}

void disp_packetCount(uint8_t total, uint8_t sent, uint8_t saved)
{
  StaticJsonDocument<500> doc;
  doc["displayCommand"] = PACKETCOUNT;
  doc["total"] = total;
  doc["sent"] = sent;
  doc["saved"] = saved;
  serializeJson(doc, serialDisplay);
}

void disp_connectingToInternet(internet_t state)
{
  StaticJsonDocument<500> doc;
  doc["displayCommand"] = INTERNET;
  doc["state"] = state;
  serializeJson(doc, serialDisplay);
}

void disp_updateNetwork(bool state)
{
  StaticJsonDocument<500> doc;
  doc["displayCommand"] = SIGNAL;
  doc["state"] = state;
  serializeJson(doc, serialDisplay);
}

void disp_sendState(sendState_t state, String estate, String weight, String timestamp)
{
  StaticJsonDocument<500> doc;
  doc["displayCommand"] = SENDSTATE;
  doc["state"] = state;
  doc["estate"] = estate;
  doc["weight"] = weight;
  doc["timestamp"] = timestamp;
  serializeJson(doc, serialDisplay);
}

void disp_sendOrDelete(sendOrDelete_t state)
{
  StaticJsonDocument<500> doc;
  doc["displayCommand"] = SENDORDELETE;
  doc["state"] = state;
  serializeJson(doc, serialDisplay);
}

void disp_deleteConfirmation(confirmDelete_t state, String estate, String weight, String timestamp)
{
  StaticJsonDocument<500> doc;
  doc["displayCommand"] = CONFIRMDELETE;
  doc["state"] = state;
  doc["estate"] = estate;
  doc["weight"] = weight;
  doc["timestamp"] = timestamp;
  serializeJson(doc, serialDisplay);
}

void disp_selectEstate(String estate)
{
  StaticJsonDocument<500> doc;
  doc["displayCommand"] = SELECTESTATE;
  doc["estate"] = estate;
  serializeJson(doc, serialDisplay);
}
